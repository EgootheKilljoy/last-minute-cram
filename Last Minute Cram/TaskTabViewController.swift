//
//  TaskTabViewController.swift
//  Last Minute Cram
//
//  Created by Pool,Adam E on 10/7/16.
//  Copyright © 2016 Underachieving Heroes. All rights reserved.
//

import UIKit

class TaskTabViewController: UIViewController {

    @IBOutlet weak var taskNameInputTF: UITextField!
    
    @IBOutlet weak var taskDescriptionTV: UITextView!
    @IBOutlet weak var taskOutputTV: UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        taskDescriptionTV.layer.borderWidth = 0.5
        taskDescriptionTV.layer.borderColor = borderColor.CGColor
        taskDescriptionTV.layer.cornerRadius = 5.0
        
        


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addTask(sender: AnyObject){
        if taskNameInputTF.text! == "Name of Task" || taskNameInputTF.text! == ""{
            displayMessage("Please enter the name of your task")
        
        }
        
        else if taskDescriptionTV.text! == "Description" || taskDescriptionTV.text! == ""{
            displayMessage("Please enter a description")
        
        }
        
        else{
            taskOutputTV.text.appendContentsOf(taskNameInputTF.text!+":")
            taskOutputTV.text.appendContentsOf("\n  ")
            taskOutputTV.text.appendContentsOf(taskDescriptionTV.text!)
            taskNameInputTF.text = ""
            taskDescriptionTV.text = ""
            taskOutputTV.text.appendContentsOf("\n")
        }
    
    }
    
    func displayMessage(message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    
    }
