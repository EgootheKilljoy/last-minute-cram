//
//  NotesViewController.swift
//  Last Minute Cram
//
//  Created by Pool,Adam E on 10/7/16.
//  Copyright © 2016 Underachieving Heroes. All rights reserved.
//

import UIKit

class NotesViewController: UIViewController {

    
    @IBOutlet weak var noteInputTV: UITextView!
    
    @IBOutlet weak var noteOutputTV: UITextView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        noteInputTV.layer.borderWidth = 0.5
        noteInputTV.layer.borderColor = borderColor.CGColor
        noteInputTV.layer.cornerRadius = 5.0

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendNotes(sender: AnyObject){
        if noteInputTV.text! == "Input Notes Here" || noteInputTV.text! == ""{
            displayMessage("Please enter some materials.")
        }
        else{noteOutputTV.text?.appendContentsOf(noteInputTV.text!)
            noteInputTV.text = ""
            noteOutputTV.text.appendContentsOf("\n\n")
        }
        
    
    }
    
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    
}
